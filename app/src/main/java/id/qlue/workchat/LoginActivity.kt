package id.qlue.workchat

import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.mesibo.api.Mesibo
import cz.msebera.android.httpclient.Header
import id.qlue.workchat.databinding.ActivityLoginBinding
import org.json.JSONObject
import java.lang.Exception

class LoginActivity : AppCompatActivity(), Mesibo.ConnectionListener, View.OnClickListener {

    private lateinit var binding: ActivityLoginBinding
    private var userList= ArrayList<User>()
    private var loginList= ArrayList<User>()
    companion object{
        private val TAG = LoginActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initConnectionUser()

        binding.btnLogin.setOnClickListener(this)

    }

    private fun initConnectionUser(){
        val list = ArrayList<User>()
        val client = AsyncHttpClient()
        val url =
            ""
        client.get(url, object : AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>,
                responseBody: ByteArray
            ) {

                val result = String(responseBody)
                Log.d(TAG, result)
                try {
                    val responseObject = JSONObject(result)
                    val listUsers = responseObject.getJSONArray("users")

                    for (position in 0 until listUsers.length()) {
                        val user = listUsers.getJSONObject(position)
                        val address = user.getString("address")
//                        if (address == LoginActivity.loginUser){
//                            continue
//                        }
                        val token = user.getString("token")
                        val online = user.getString("online")
                        val lastOnline = user.getString("lastonline")
                        val statusLastOnline = when (lastOnline.toInt() > 1_600_000_000) {
                            true -> "Never Login Yet"
                            false -> "online $lastOnline seconds ago"
                        }
                        val statusOnline = when (online.toInt()) {
                            1 -> "Status: Online"
                            else -> "Status: Offline"
                        }

                        val contact = User(
                            token,
                            "",
                            address,
                            statusLastOnline,
                            statusOnline
                        )
                        list.add(contact)
                    }
                     userList.addAll(list)

                } catch (e: Exception) {
                    Log.d("Exception", e.message.toString())
                }
            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>,
                responseBody: ByteArray,
                error: Throwable
            ) {
                val errorMessage = when (statusCode) {
                    401 -> "$statusCode : Bad Request"
                    403 -> "$statusCode : Forbidden"
                    404 -> "$statusCode : Not Found"
                    else -> "$statusCode : ${error.message}"
                }
                Toast.makeText(this@LoginActivity, errorMessage, Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun initUserLogin(user: User) {
        val api: Mesibo = Mesibo.getInstance()
        api.init(applicationContext)
        val profile = Mesibo.UserProfile()
        profile.address = user.address
        Mesibo.addListener(this)
        Mesibo.setSecureConnection(true)
        Mesibo.setAccessToken(user.token)
        Mesibo.setUserProfile(profile,false)
        Mesibo.setDatabase("mydb", 0)

        Mesibo.start()
    }

    override fun Mesibo_onConnectionStatus(status: Int) {
        when (status) {
            1 -> {
                val intent = Intent(this@LoginActivity, ContactListActivity::class.java)
                intent.putParcelableArrayListExtra(ContactListActivity.EXTRA_LIST_USER,loginList)
                Toast.makeText(this,"Login Success",Toast.LENGTH_SHORT).show()
                startActivity(intent)

//                val pendingIntent = TaskStackBuilder.create(this)
//                    .addParentStack(MainActivity::class.java)
//                    .addNextIntent(intent)
//                    .getPendingIntent(110, PendingIntent.FLAG_UPDATE_CURRENT)
//                startActivity(pendingIntent)
            }
            4 -> binding.tvConnStatus.text = "Connection Status: Connection Failed"
            5 -> binding.tvConnStatus.text = "Connection Status: Connection Failed"
            else -> {
                binding.tvConnStatus.text = "Connection Status: Connecting..."

            }
        }
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btn_login){
            if (binding.edtUserAddress.text.isNotEmpty()){
                val userAddress = binding.edtUserAddress.text.toString()
                val user = userList.find { it.address == userAddress }
                if (user!=null){
                    //sebelum init user login dibuang
                    val list = ignoreUserLogin(user,userList)
                    loginList.addAll(list)
                    //aktifkan terus intent kan
                    initUserLogin(user)
                    binding.btnLogin.isEnabled = false

                } else {
                    binding.edtUserAddress.error = "User address not found"
                }
            } else {
                binding.edtUserAddress.error = "Field must not empty"
            }
        }
    }

    private fun ignoreUserLogin(user: User, list: ArrayList<User>): ArrayList<User>{
        val copyList= ArrayList<User>()
        for (data in list){
            if (user.address == data.address){
                continue
            }
            copyList.add(data)
        }
        return copyList
    }


}