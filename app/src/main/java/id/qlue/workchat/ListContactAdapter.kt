package id.qlue.workchat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.qlue.workchat.databinding.ItemContactBinding

class ListContactAdapter: RecyclerView.Adapter<ListContactAdapter.ListViewHolder>() {

    private val mData= ArrayList<User>()
    private var onItemClickCallback: OnItemClickCallback?= null

    fun setData(items: ArrayList<User>){
        mData.clear()
        mData.addAll(items)
        notifyDataSetChanged()
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback){
        this.onItemClickCallback = onItemClickCallback
    }


    inner class ListViewHolder(private val binding: ItemContactBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(user: User){
            with(binding){
//                Glide.with(itemView.context)
//                    .load(contact.photo)
//                    .apply(RequestOptions().override(55,55))
//                    .into(imgContactPhoto)

                tvName.text = user.name
                tvAddress.text = user.address
                tvOnlineStatus.text = user.onlineStatus
                tvLastOnline.text = user.lastOnline

                itemView.setOnClickListener { onItemClickCallback?.onItemClicked(user) }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListViewHolder {
        val binding = ItemContactBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    override fun getItemCount(): Int = mData.size

    interface OnItemClickCallback{
        fun onItemClicked(data:User)
    }
}