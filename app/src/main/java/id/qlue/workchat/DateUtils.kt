package id.qlue.workchat

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    fun fromMilisToTimeString(milis: Long?):String{
        val format = SimpleDateFormat("hh:mm a", Locale.getDefault())
        return format.format(milis)
    }
}