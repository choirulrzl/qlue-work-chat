package id.qlue.workchat

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    var token: String,
    var name: String = "",
    var address: String,
    var lastOnline: String,
    var onlineStatus: String
) : Parcelable
