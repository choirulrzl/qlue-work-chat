package id.qlue.workchat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.mesibo.api.Mesibo
import id.qlue.workchat.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding



    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityMainBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)


    }




}