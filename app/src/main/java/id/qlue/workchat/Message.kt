package id.qlue.workchat

data class Message(
    var isFromOtherUser: Boolean,
    var message: String = "",
    var time: Long? = 0L
)
