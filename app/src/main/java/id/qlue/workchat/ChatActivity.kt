package id.qlue.workchat

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import android.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.mesibo.api.Mesibo
import id.qlue.workchat.databinding.ActivityChatBinding
import java.lang.Exception
import java.util.*

class ChatActivity : AppCompatActivity(), Mesibo.MessageListener, Mesibo.FileTransferHandler, Mesibo.FileTransferListener {

    private lateinit var binding: ActivityChatBinding
    private lateinit var adapter: MessageAdapter
    private lateinit var remoteUser: User
    private var mParameter: Mesibo.MessageParams? = null
    private var mLastMessageId: Long = 0

    companion object{
        private val TAG = ChatActivity::class.java.simpleName
        const val EXTRA_REMOTE_USER = "extra_remote_user"
        val REQUEST_CODE = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChatBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val toolbar = binding.toolbar
        toolbar.inflateMenu(R.menu.option_menu)

        //get the user data clicked from contact list
        remoteUser = intent.getParcelableExtra<User>(EXTRA_REMOTE_USER) as User
        readSession(remoteUser)

        binding.tvChatName.text = remoteUser.name
        binding.tvChatStatus.text = remoteUser.onlineStatus

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.btnSend.setOnClickListener {
            if (binding.edtTextMessage.text.isNotEmpty()){

                //Send Message by API Mesibo here
                //class message dengan id pengirim App.useraddress dikirim ke remoteuser
                sendMessage(remoteUser,binding.edtTextMessage.text.toString().trim())
                //call reset input if finish send
            } else {
                Toast.makeText(applicationContext,"Message shouldn't be empty", Toast.LENGTH_SHORT).show()
            }
        }

        binding.btnImagePick.setOnClickListener {
            openGalleryImage()
        }

        binding.rvMessageList.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,true)
        adapter = MessageAdapter(this)
        binding.rvMessageList.adapter = adapter

    }

    private fun openGalleryImage() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
            //TODO: implementatation image path here
//            imageView.setImageURI(data?.data) // handle chosen image
        }
    }

    private val messageId: Long
        private get() {
            mLastMessageId = Mesibo.random()
            return mLastMessageId
        }

    private fun sendImage(caption: String,filepath: String,bmp: Bitmap){
        val mId: Long = messageId
        var filetype: Int = Mesibo.FileInfo.TYPE_AUTO

        val file = Mesibo.getFileInstance(mParameter, mId, Mesibo.FileInfo.MODE_UPLOAD, filetype, Mesibo.FileInfo.SOURCE_MESSAGE, filepath, null, this)
        val m = Mesibo.MessageData


    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.option_menu,menu)
        return true
    }


    private fun sendMessage(peerUser: User, message: String) {
        val otherUser = Mesibo.MessageParams()
        otherUser.peer = peerUser.address

        Mesibo.sendMessage(otherUser, Mesibo.random(), message)
        resetInput()
        adapter.notifyDataSetChanged()
    }

    private fun resetInput(){
        //clean text box
        binding.edtTextMessage.text.clear()

        //hide keyboard
        val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(
            currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS
        )
    }




    private fun readSession(peerUser: User){
        Mesibo.setAppInForeground(this,0,true)
        val readSession = Mesibo.ReadDbSession(peerUser.address,this)
        readSession.enableReadReceipt(true)
        readSession.read(100)
    }

    //method for read message
    override fun Mesibo_onMessage(messageParams: Mesibo.MessageParams?, data: ByteArray): Boolean {
        try {
            val timestamp = messageParams?.ts
            val isFromOtherUser =
                messageParams?.status == Mesibo.MSGSTATUS_RECEIVEDNEW || messageParams?.status == Mesibo.MSGSTATUS_RECEIVEDREAD
            val message = String(data)

            val readMessage = Message(
                isFromOtherUser,
                message,
                timestamp
            )


            adapter.addMessage(readMessage)

        } catch (e: Exception) {
            Log.d(TAG, e.message.toString())
        }
        return true
    }

    override fun Mesibo_onMessageStatus(p0: Mesibo.MessageParams?) {

    }

    override fun Mesibo_onActivity(p0: Mesibo.MessageParams?, p1: Int) {

    }

    override fun Mesibo_onLocation(p0: Mesibo.MessageParams?, p1: Mesibo.Location?) {

    }

    override fun Mesibo_onFile(p0: Mesibo.MessageParams?, p1: Mesibo.FileInfo?) {

    }

    override fun Mesibo_onStartFileTransfer(p0: Mesibo.FileInfo?): Boolean {
        TODO("Not yet implemented")
    }

    override fun Mesibo_onStopFileTransfer(p0: Mesibo.FileInfo?): Boolean {
        TODO("Not yet implemented")
    }

    override fun Mesibo_onFileTransferProgress(p0: Mesibo.FileInfo?): Boolean {
        TODO("Not yet implemented")
    }
}