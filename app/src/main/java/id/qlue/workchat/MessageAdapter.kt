package id.qlue.workchat

import android.content.Context

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.qlue.workchat.databinding.ActivityChatBinding
import id.qlue.workchat.databinding.MyMessageBinding
import id.qlue.workchat.databinding.OtherMessageBinding


class MessageAdapter(val context: Context): RecyclerView.Adapter<MessageAdapter.MessageViewHolder>() {

    private val listMessages = ArrayList<Message>()

    companion object{
        private const val VIEW_TYPE_MY_MESSAGE = 1
        private const val VIEW_TYPE_OTHER_MESSAGE = 2
    }

    fun addMessage(message: Message){
        listMessages.add(message)
        notifyDataSetChanged()
    }

    open class MessageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        open fun bind(message: Message){}
    }

    inner class MyMessageViewHolder(view: View): MessageViewHolder(view){
        private val binding = MyMessageBinding.bind(view)
        override fun bind(message: Message) {
            super.bind(message)
            binding.tvMyMessage.text = message.message
            binding.tvMyMessageTime.text = DateUtils.fromMilisToTimeString(message.time)
        }
    }

    inner class OtherMessageViewHolder(view: View): MessageViewHolder(view){
        private val binding = OtherMessageBinding.bind(view)
        override fun bind(message: Message) {
            super.bind(message)
            binding.tvOtherMessage.text = message.message
            binding.tvOtherTime.text = DateUtils.fromMilisToTimeString(message.time)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        return if (viewType == VIEW_TYPE_MY_MESSAGE){
            MyMessageViewHolder(
                LayoutInflater.from(context).inflate(R.layout.my_message, parent, false)
            )
        } else {
            OtherMessageViewHolder(
                LayoutInflater.from(context).inflate(R.layout.other_message,parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val message = listMessages[position]
        holder.bind(message)
    }

    override fun getItemViewType(position: Int): Int {
        super.getItemViewType(position)
        val message = listMessages[position]

        return if (message.isFromOtherUser){
            VIEW_TYPE_OTHER_MESSAGE
        } else {
            VIEW_TYPE_MY_MESSAGE
        }
    }

    override fun getItemCount(): Int = listMessages.size


}