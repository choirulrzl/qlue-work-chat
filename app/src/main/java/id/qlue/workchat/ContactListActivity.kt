package id.qlue.workchat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.mesibo.api.Mesibo
import com.mesibo.messaging.MesiboUI
import cz.msebera.android.httpclient.Header
import id.qlue.workchat.databinding.ActivityContactListBinding
import org.json.JSONObject
import java.lang.Exception

class ContactListActivity : AppCompatActivity(){
    private lateinit var binding: ActivityContactListBinding
    private lateinit var adapter: ListContactAdapter
    private lateinit var userList: ArrayList<User>




    companion object {
        const val EXTRA_LIST_USER = "extra_list_user"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityContactListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title = "Contact List"

        userList = intent.getSerializableExtra(EXTRA_LIST_USER) as ArrayList<User>


        binding.rvListContact.setHasFixedSize(true)
        adapter = ListContactAdapter()
        adapter.notifyDataSetChanged()
        adapter.setData(userList)
        showRecycleListContact()
    }

    private fun showRecycleListContact() {
        binding.rvListContact.layoutManager = LinearLayoutManager(this)
        binding.rvListContact.adapter = adapter

        adapter.setOnItemClickCallback(object : ListContactAdapter.OnItemClickCallback {
            override fun onItemClicked(data: User) {
                val intentToChat = Intent(this@ContactListActivity, ChatActivity::class.java)
                intentToChat.putExtra(ChatActivity.EXTRA_REMOTE_USER,data)
                startActivity(intentToChat)
            }
        })
    }


}